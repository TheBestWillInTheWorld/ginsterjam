//
//  LoginViewController.swift
//  Ginstergram
//
//  Created by Will Gunby on 19/11/2014.
//  Copyright (c) 2014 Will Gunby. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, AsyncInteraction {

    @IBOutlet var txtUsername: UITextField!
    @IBOutlet var txtPassword: UITextField!
    
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var btnSignup: UIButton!
    
    @IBOutlet var btnImNew: UIButton!
    
    var offScreen:CGPoint!
    var onScreen:CGPoint!
    
    var backend = BWBackend()
    
    //TODO: if logged in, offer logout
    //      would probably be worth having a profile details section either in here or replacing this while logged in
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        onScreen = btnLogin.center
        offScreen = btnSignup.center
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }
    
    var spinny = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50, 50))
    
    @IBAction func btnLogin_up(sender: AnyObject) {
        if validateLogin() {
            backend.asyncDelegate = self
            backend.login(txtUsername.text, password: txtPassword.text)
        }
    }
    
    @IBAction func btnSignup_up(sender: AnyObject) {
        if validateLogin() {
            backend.asyncDelegate = self
            
            backend.signup(txtUsername.text, password: txtPassword.text, firstname: "", lastname: "", nickname: "", email: txtUsername.text, sundryValues: NSDictionary())
        }
    }
    
    
    func validateLogin() -> Bool{
        //use validation to reduce the amount of API calls made
        var title:String = ""
        var message:String = ""
        if BWBackend.validateLogin(txtUsername.text, password: txtPassword.text, validationTitle: &title, validationMessage: &message) {
            return true
        }
        else{
            var alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
                alert.dismissViewControllerAnimated(true, completion: nil)
            }))
            self.presentViewController(alert, animated: true, completion: nil)
                return false
        }
    }
    
    
    @IBAction func btnCancel_up(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    var showingLogin=true
    @IBAction func btnImNew(sender: AnyObject) {
        //was going to animate the login button swapping with the signup, but I can't get the animation to work how I want...
        if self.showingLogin {
            self.btnSignup.center = self.onScreen
            self.btnLogin.center = self.offScreen
        }
        else                 {
            self.btnLogin.center = self.onScreen
            self.btnSignup.center = self.offScreen
        }
        self.showingLogin = !self.showingLogin
    }

    
    
    
    
    
    
    func beginAsyncWait(callName:String){
        spinny.center = self.view.center
        spinny.hidesWhenStopped = true
        spinny.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        self.view.addSubview(spinny)
        spinny.startAnimating()
        //UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    func endAsyncWaitWithSuccess(callName:String, data:[AnyObject]?){
        spinny.stopAnimating()
        //auto login on signup
        if callName == "signup" {btnLogin_up(self)}
        if callName == "login" {
            self.navigationController?.popViewControllerAnimated(true)
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }   
    func endAsyncWaitWithError(callName:String, error: NSError){
        spinny.stopAnimating()
        //need to handle a few cases
        /*
            tried login > invalid = signup/try again
            tried login > invalid - tried signup > user exists = suggest they check their email/password & try again
            tried signup > user exists = suggest they check their password & try again
            tried login for same user twice = offer password recovery
        */
        
        var errortext = error.userInfo?["error"] as String
        var alert = UIAlertController(title: "Oh no!", message: errortext, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { action in
            alert.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        //if the login was invalid, suggest a signup (could be dangerous as people might make loads of account based on typos/wrong passwords!)
        if errortext.lowercaseString.rangeOfString("invalid login") != nil {
            alert.addAction(UIAlertAction(title: "Sign up?", style: .Default, handler: { action in
                self.btnSignup_up(self)
            }))
        }
        self.presentViewController(alert, animated: true, completion: nil)
        
    }

}

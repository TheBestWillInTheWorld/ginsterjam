//
//  FeedTableViewCell.swift
//  GinsterJam
//
//  Created by Will Gunby on 24/11/2014.
//  Copyright (c) 2014 Will Gunby. All rights reserved.
//

import UIKit

class FeedTableViewCell: UITableViewCell {

    @IBOutlet var lblUser: UILabel!
    @IBOutlet var imgPreview: UIImageView!
    @IBOutlet var lblMessage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

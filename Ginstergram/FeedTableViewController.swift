//
//  FeedTableViewController.swift7
//  GinsterJam
//
//  Created by Will Gunby on 24/11/2014.
//  Copyright (c) 2014 Will Gunby. All rights reserved.
//

import UIKit
import Parse

class FeedTableViewController: UITableViewController, AsyncInteraction {
    
    var backend = BWGinsterJam()
    var watchedUsers = [String]()
    var refreshPull: UIRefreshControl!
    var posts = Dictionary<String, BWPost>()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        checkLogin()
        refreshPull = UIRefreshControl()
        refreshPull.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshPull.addTarget(self, action: "refresh", forControlEvents: .ValueChanged)
        self.tableView.addSubview(refreshPull)
        
        refresh()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var refreshing = false
    override func viewDidAppear(animated: Bool) {
        if refreshing {
            refresh()
        }
    }
    
    func refresh(){
        refreshing=true
        if checkLogin() {
            posts = Dictionary<String, BWPost>()
            backend.fetchWatchedUserPosts()
            self.refreshPull.endRefreshing()
            refreshing=false
        }
    }
    
    //TODO: if you don't have any watched people, probably go straight to the people page
    
    func checkLogin() -> Bool{
        if backend.isLoggedIn() {
            backend.asyncDelegate = self
            return true
        }
        else {
            var alert = UIAlertController(title: "Oh no!", message: "You're not logged in.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { action in
                alert.dismissViewControllerAnimated(true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "Login", style: .Default, handler: { action in
                alert.dismissViewControllerAnimated(true, completion: nil)
                self.refreshing = true
                self.performSegueWithIdentifier("toLogin", sender: self)
            }))
            self.presentViewController(alert, animated: true, completion: nil)
            return false
        }
    }

    @IBAction func btnPeople_up(sender: AnyObject) {
        //force refresh on return (should really only do this when changes have been made on the othe screen, but this will do for now
        refreshing=true
        //manually triggered segue
        self.performSegueWithIdentifier("toPeople", sender: self)
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return posts.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as FeedTableViewCell
        let index = indexPath.row
        
        //if isRowZeroVisible(index){
        var post:BWPost? = posts[String(index)]
        println(posts)
        if post != nil {
            setCellFromPost(&cell, post: post)
        }
        
        return cell
    }
    
    func setCellFromPost(inout cell:FeedTableViewCell, post:BWPost!){
        cell.lblUser.text = post!.user
        cell.lblMessage.text = post!.message
        if post!.image == nil {
            //post!.getImageAsync(indexPath.row, closure: asyncImageLoad)
        }
        else {  cell.imgPreview.image = post!.image  }

    }
    
    //TODO: bugs all over the place in the image loading:
    /*
            visible cells don't load the image out of line
            some cells get the wrong images
                --maybe wait for all images before reloading the whole thing? - seems a bit crap
    */
    
    
    func asyncImageLoad(index:Int, bwPost:BWPost){
        let indexPath = NSIndexPath(index: index)
        var cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as FeedTableViewCell
        //cell.imgPreview.image = bwPost.image
        setCellFromPost(&cell, post: bwPost)
        
        //self.tableView.reloadData()
        self.tableView.beginUpdates()
        self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.None)
        self.tableView.endUpdates()
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 260
    }
    
    func getFeed(){
        var filters = Dictionary<String, String>()
        backend.fetch("post", filters: filters)
    }
    
    
    var spinny = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50, 50))
    func beginAsyncWait(callName:String){
        if callName != "getImageAsync" {
            spinny.center = self.view.center
            spinny.hidesWhenStopped = true
            spinny.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
            self.view.addSubview(spinny)
            spinny.startAnimating()
            UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        }
    }
    func endAsyncWaitWithSuccess(callName:String, data:[AnyObject]?){
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        spinny.stopAnimating()
        
        if callName == "fetch" {
            //handle the data 
            //could to with classname appearing in here!
            
        }
        if callName != "getImageAsync" {
        }
        if callName == "fetchWatchedUserPosts" {
            var index = 0
            //clear posts
            posts = Dictionary<String, BWPost>()
            tableView.clearsContextBeforeDrawing = true
            for item in data as [PFObject] {
                var post = BWPost(index:index, parseObject: item, asyncDelegate:self)
                posts["\(post.index!)"] = post
                post.getImageAsync(index, closure: asyncImageLoad)
                index++
            }
            self.tableView.reloadData()            
        }
        if callName == "fetchWatchedUsers" {
            watchedUsers = data as [String]
            self.tableView.reloadData()
        }
    }
    func endAsyncWaitWithError(callName:String, error: NSError){
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        spinny.stopAnimating()
        
        var errortext = error.userInfo?["error"] as String
        var alert = UIAlertController(title: "Oh no!", message: errortext, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
            alert.dismissViewControllerAnimated(true, completion: nil)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
        
    }


    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as UITableViewCell

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}



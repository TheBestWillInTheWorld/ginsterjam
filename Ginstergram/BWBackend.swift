//
//  Parse.swift
//  Ginstergram
//
//  Created by Will Gunby on 18/11/2014.
//  Copyright (c) 2014 Will Gunby. All rights reserved.
//

import Foundation
import Parse

protocol AsyncInteraction{
    func beginAsyncWait(callName:String)
    func endAsyncWaitWithSuccess(callName:String, data:[AnyObject]?)
    func endAsyncWaitWithError(callName:String, error: NSError)
    //TODO: find a way to get current method name in order to genericise the callName parameter passed back up the chain
}

//TODO: split user calls from object calls
class BWBackend:NSObject {
    // the aim of this class is to abstract the app away from Parse so you can change the backend relatively smoothly in anything that uses this library
    //  it does mean that many of the functions echo the structure of the base Parse calls though...
    //          currently flawed as I'm passing back PArse object types fromt the calls
    
    //https://parse.com/docs/ios_guide#users/iOS
    
    var asyncDelegate:AsyncInteraction? = nil
    
    class func isValidEmail(testStr:String) -> Bool {
        println("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        var emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest!.evaluateWithObject(testStr)
        return result
    }
    
    class func validateLogin(email:String, password:String, inout validationTitle:String, inout validationMessage:String) -> Bool{
            //use validation to reduce the amount of API calls made
            if email == "" {
                validationTitle = "Email address missing"
                validationMessage = "please enter your email address"
            }
            else if !BWBackend.isValidEmail(email) {
                validationTitle = "Email address"
                validationMessage = "please enter a valid email address"
            }
            else if password == "" {
                validationTitle = "Password missing"
                validationMessage = "please enter your password"
            }
            else {
                return true
            }
        return false
    }
    
    func signup(username:String, password:String, callName:String = "signup"){
        //this oveloaded function will preserve an anon user's details and convert them to a "real" user by seeeing username/pwd
        //  alternaticvely, it's the most basic signup for a new user...
        var user:PFUser
        if isAnonUser() { user = PFUser.currentUser() }
        else {user = PFUser()}
        signup(username, password:password, user:user,callName:callName)
    }
    
    func signup(username:String, password:String, firstname: String, lastname: String, nickname: String, email: String, sundryValues: NSDictionary, callName:String = "signup") {
        //this oveloaded function will overwrite an anon user's details as supplied here
        
        var user:PFUser
        if isAnonUser() { user = PFUser.currentUser() }
        else {user = PFUser()}
        
        user.email = email
        
        // this might be asking for trouble! (values hidden away in here) - might be better to sotore as individual fields, but that would risk very wide tables...
        user["sundryValues"] = sundryValues
        signup(username, password:password, user:user,callName:callName)
    }
    
    func signup(username:String, password:String, user:PFUser, callName:String = "signup") {
        //doing it this way makes converting from an anon user to a "real" user nice and simple
        user.username = username
        user.password = password
        
        if asyncDelegate != nil {asyncDelegate?.beginAsyncWait(callName)}
        user.signUpInBackgroundWithBlock {
            (succeeded: Bool!, error: NSError!) -> Void in
            if error == nil {
                // Hooray! Let them use the app now.
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithSuccess(callName, data:nil)}
            } else {
                //let errorString:String? = error.userInfo["error"]
                // Show the errorString somewhere and let the user try again.
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithError(callName,error: error)}
                
            }
        }
    }
    
    
    func login(username:String, password:String, callName:String = "login"){
        
        if asyncDelegate != nil {asyncDelegate?.beginAsyncWait(callName)}
        PFUser.logInWithUsernameInBackground(username, password:password) {
            (user: PFUser!, error: NSError!) -> Void in
            if user != nil {
                // Do stuff after successful login.
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithSuccess(callName,data:nil)}
            } else {
                // The login failed. Check error to see why.
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithError(callName,error: error)}
            }
        }
    }
    
    func login(callName:String = "login"){
        if asyncDelegate != nil {asyncDelegate?.beginAsyncWait(callName)}
        PFAnonymousUtils.logInWithBlock {
            (user: PFUser!, error: NSError!) -> Void in
            if error != nil {
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithSuccess(callName,data:[user])}
                NSLog("Anonymous login failed.")
            } else {
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithError(callName,error: error)}
                NSLog("Anonymous user logged in.")
            }
        }
    }
    
    func login(authToken:String, callName:String = "login"){
        if asyncDelegate != nil {asyncDelegate?.beginAsyncWait(callName)}
        PFUser.becomeInBackground(authToken, {
            (user: PFUser!, error: NSError!) -> Void in
            if error != nil {
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithSuccess(callName,data:[user])}
                NSLog("login from auth token failed.")
            } else {
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithError(callName,error: error)}
                NSLog("Auth token logged in.")
            }
        })
    }
    
    func loggedInUsername() -> String {
        if isLoggedIn(){
            return PFUser.currentUser().username
        }
        return ""
    }
    
    func isLoggedIn() -> Bool{
        //if there's a persisted user object, it's safe to assume it has a valid auth token
        //  only delare them as logged in if they're not using an anonymous user
        return PFUser.currentUser() != nil && !isAnonUser()
    }
    
    func isAnonUser() -> Bool {
        return  PFAnonymousUtils.isLinkedWithUser(PFUser.currentUser())
    }
    
    func logOut(){
        PFUser.logOut()
    }
    
    func fetchUsers(excludeCurrentUser:Bool = true, callName:String = "fetchUsers"){
        if asyncDelegate != nil {asyncDelegate?.beginAsyncWait(callName)}
        var userQuery = PFUser.query()
        userQuery.findObjectsInBackgroundWithBlock { (pfUsers: [AnyObject]!, error: NSError!) -> Void in
            if error == nil {
                var users = [String]()
                for user:PFUser in pfUsers as [PFUser] {
                    if user.username != PFUser.currentUser().username || !excludeCurrentUser {
                       users.append(user.username)
                    }
                }
                
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithSuccess(callName,data:users)}
            } else {
                //let errorString:String? = error.userInfo["error"]
                // Show the errorString somewhere and let the user try again.
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithError(callName,error: error)}
                
            }
        }
    }
        
    ///////////////////
    //password reset:
    //
    //PFUser.requestPasswordResetForEmailInBackground("email@example.com")
    

    ///////////////////
    //user query
    /*
    var query = PFUser.query()
    query.whereKey("gender", equalTo:"female")
    var girls = query.findObjects()
    */
    
    ///////////////////
    //associating public objects with users for querying:
    /*
    PFUser *user = [PFUser currentUser];

    // Make a new post
    PFObject *post = [PFObject objectWithClassName:@"Post"];
    post[@"title"] = @"My New Post";
    post[@"body"] = @"This is some great content.";
    post[@"user"] = user;
    [post save];

    // Find all posts by the current user
    PFQuery *query = [PFQuery queryWithClassName:@"Post"];
    [query whereKey:@"user" equalTo:user];
    NSArray *usersPosts = [query findObjects];
    */
    
    
    func saveUser(username:String, password:String, firstname: String, lastname: String, nickname: String, email: String, sundryValues: NSDictionary, callName:String = "saveUser") {
        var user:PFUser
        if isAnonUser() {
            //if it's an anon user, we need to convert them first using signup
            signup(username, password: password, firstname: firstname, lastname: lastname, nickname: nickname, email: email, sundryValues: sundryValues)
            return
        }
        else {user = PFUser()}
        
        user.email = email
        user.username = username
        user.password = password
        
        if asyncDelegate != nil {asyncDelegate?.beginAsyncWait(callName)}
        user.saveInBackgroundWithBlock {
            (succeeded: Bool!, error: NSError!) -> Void in
            if error == nil {
                // Hooray! Let them use the app now.
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithSuccess(callName,data:nil)}
            } else {
                //let errorString:String? = error.userInfo["error"]
                // Show the errorString somewhere and let the user try again.
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithError(callName,error: error)}
                
            }
        }
    }
        
    func deleteItem(item:PFObject!){        deleteItem(item,callName:"deleteItem")    }
    func deleteItem(item:PFObject!, callName:String){        deleteItems([item],callName:callName)    }
    
    func deleteItems(items:[AnyObject]!){        deleteItems(items,callName:"deleteItems")    }
    func deleteItems(items:[AnyObject]!, callName:String){
        for item in items as [PFObject] {
            if asyncDelegate != nil {asyncDelegate?.beginAsyncWait(callName)}
            item.deleteInBackgroundWithBlock({ (success:Bool, error:NSError!) -> Void in
                if error == nil {
                    if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithSuccess(callName,data:[item])}
                }
                else {
                    println(error)
                    if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithError(callName,error: error)}
                }
            })
        }
    }
    
    
    //permit calls without a real closure specified
    func fetchOne(className:String, filters:Dictionary<String, String>!, callName:String = "fetchOne"){
        var closure :(items:PFObject!) -> () = {(items:PFObject!) -> () in
            return}
        fetchOne(className, filters: filters, closure: closure,callName:callName)
    }
    //closure is included to allow internal backend functions to handle some of the output - e.g. delete
    func fetchOne(className:String, filters:Dictionary<String, String>!, closure:(item:PFObject!)->() , callName:String = "fetchOne"){
        
        var query = buildQuery(className, filters:filters)
        
        if asyncDelegate != nil {asyncDelegate?.beginAsyncWait(callName)}
        query.getFirstObjectInBackgroundWithBlock( {(item:PFObject!, error: NSError!) -> Void in
            if error == nil {
                println("Success.  Object found for Id: \(item.objectId)")
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithSuccess(callName,data:[item])}
                closure(item: item)
            }
            else {
                println(error)
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithError(callName,error: error)}
            }
        })
        
    }
    
    //permit calls without a real closure specified
    func fetch(className:String, filters:Dictionary<String, String>!, callName:String = "fetch"){
        var closure :(items:[AnyObject]!) -> () = {(items:[AnyObject]!) -> () in
                                                    return}
        fetch(className, filters: filters, closure: closure,callName:callName)
    }
    
    
    //closure is included to allow internal backend functions to handle some of the output - e.g. delete
    func fetch(className:String, filters:Dictionary<String, String>!, closure:(items:[AnyObject]!)->(), callName:String = "fetch"){
        
        var query = buildQuery(className, filters:filters)
        query.orderByDescending("createdAt")
        
        if asyncDelegate != nil {asyncDelegate?.beginAsyncWait(callName)}
        query.findObjectsInBackgroundWithBlock( {(items:[AnyObject]!, error: NSError!) -> Void in
            if error == nil {
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithSuccess(callName,data:items)}
                closure(items: items)
            }
            else {
                println(error)
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithError(callName,error: error)}
            }
        })
    }
    
    //permit calls without a real closure specified
    func fetch(className:String, filters:Dictionary<String, String>!, joinFromField:String, joinToField:String, joinTargetClass:String, joinTargetFilters:Dictionary<String, String>!, callName:String = "fetch"){
        var closure :(items:[AnyObject]!) -> () = {(items:[AnyObject]!) -> () in
            return}
        fetch(className, filters: filters, joinFromField:joinFromField, joinToField:joinToField, joinTargetClass:joinTargetClass, joinTargetFilters:joinTargetFilters, closure: closure, callName:callName)
    }
    
    //closure is included to allow internal backend functions to handle some of the output - e.g. delete
    func fetch(className:String, filters:Dictionary<String, String>!, joinFromField:String, joinToField:String, joinTargetClass:String, joinTargetFilters:Dictionary<String, String>!, closure:(items:[AnyObject]!)->(), callName:String = "fetch"){
        
        var query = buildQuery(className, filters:filters)
        var joinTargetQuery = buildQuery(joinTargetClass, filters:joinTargetFilters)
        query.whereKey(joinFromField, matchesKey:joinToField, inQuery:joinTargetQuery)
        query.orderByDescending("createdAt")
        
        if asyncDelegate != nil {asyncDelegate?.beginAsyncWait(callName)}
        query.findObjectsInBackgroundWithBlock( {(items:[AnyObject]!, error: NSError!) -> Void in
            if error == nil {
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithSuccess(callName,data:items)}
                closure(items: items)
            }
            else {
                println(error)
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithError(callName,error: error)}
            }
        })
    }
    
    
    func fetchById(className:String, objectId:String, callName:String = "fetchById"){
        var query = PFQuery(className: className)
        
        if asyncDelegate != nil {asyncDelegate?.beginAsyncWait(callName)}
        query.getObjectInBackgroundWithId(objectId, block: {(item:PFObject!, error: NSError!) -> Void in
            if error == nil {
                println("Success.  Object found for Id: \(item.objectId)")
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithSuccess(callName, data:[item])}
            }
            else {
                println(error)
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithError(callName,error: error)}
            }
        })
    }
    
    func pFObjectToNSDictionary(item:PFObject)->NSDictionary{
        //this function would allow me to seperate the app code from Parse entirely to make it easier to switch
        //      that said, a simple NSDictionary might not be the best.  PFObject is more like a datatable in .net with record of changes etc. onboard...
        return NSDictionary()
    }
    
    
    //not sure how useful this is beyond returning the created item and making the code outside slightly neater!
    //      > the main point is that it abstracts away from Parse somewhat to make it eaesier to switch providers
    func addObjectWithValues(className:String, values:Dictionary<String, AnyObject>, allowPublicRead:Bool, callName:String = "addObjectWithValues"){
        //todo: think about handling conflicts
        
        var newObject = PFObject(className: className)
        
        for key in values.keys {
            println(key)
            println(values[key])
            newObject[key] = values[key]!
        }
        
        //if specified, secure this object to the current user.
        if allowPublicRead {newObject.ACL.setPublicReadAccess(true)}
        else {newObject.ACL.setPublicReadAccess(false)}
        
        if asyncDelegate != nil {asyncDelegate?.beginAsyncWait(callName)}
        newObject.saveInBackgroundWithBlock { (success: Bool!, error: NSError!) -> Void in
            if success! {
                println("Success.  Object Id stored:\(newObject.objectId)")
                //pass control back up the chain
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithSuccess(callName, data:[newObject])}
            }
            else {
                println(error)
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithError(callName,error: error)}
            }
        }
        
    }
    
    func saveObject(object:PFObject, secureObjectToUser:Bool, callName:String = "fetchById"){
        //if specified, secure this object to the current user.
        if secureObjectToUser {object.ACL = PFACL(user: PFUser.currentUser())}
        
        if asyncDelegate != nil {asyncDelegate?.beginAsyncWait(callName)}
        object.saveInBackgroundWithBlock { (success: Bool!, error: NSError!) -> Void in
            if success! {
                println("Success.  Object Id stored:\(object.objectId)")
                //pass control back up the chain
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithSuccess(callName, data:[object])}
            }
            else {
                println(error)
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithError(callName,error: error)}
            }
        }
        
    }
    
    func buildQuery(className:String, filters:Dictionary<String, String>!) -> PFQuery{
        var query = PFQuery(className: className)
        if filters != nil {
            for filter in filters.keys {
                var value : String = filters[filter]!
                query.whereKey(filter as String, equalTo: value)
            }
        }
        return query
    }
    
    
    //more permissions stuff:
    /*
    ////////////////////////////////////
    
    var groupMessage = PFObject(className:"Message")
    var groupACL = PFACL.ACL()
    
    // userList is an NSArray with the users we are sending this message to.
    for (user : PFUser in userList) {
    groupACL.setReadAccess(true, forUser:user)
    groupACL.setWriteAccess(true, forUser:user)
    }
    
    groupMessage.ACL = groupACL
    groupMessage.saveInBackground()
    
    ////////////////////////////////////
    
    var publicPost = PFObject(className:"Post")
    var postACL = PFACL.ACLWithUser(PFUser.currentUser())
    postACL.setPublicReadAccess(true)
    publicPost.ACL = postACL
    publicPost.saveInBackground()
    */

    
    
}
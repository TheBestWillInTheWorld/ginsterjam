//
//  ViewController.swift
//  Ginstergram
//
//  Created by Will Gunby on 17/11/2014.
//  Copyright (c) 2014 Will Gunby. All rights reserved.
//

import UIKit
import Parse

class HomeViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextViewDelegate, AsyncInteraction {
    
    
    @IBOutlet var btnLogout: UIBarButtonItem!
    @IBOutlet var btnPickImage: UIButton!
    @IBOutlet var imgPreview: UIImageView!
    @IBOutlet var lblUser: UILabel!
    @IBOutlet var txtMessage: UITextView!
    
    var backend = BWGinsterJam()
    var placeholderColor:UIColor?
    
    //TODO: if logged in, show logout button, hide login
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        backend.asyncDelegate = self
        placeholderColor = imgPreview.backgroundColor!
    }
    
    override func viewDidAppear(animated: Bool) {
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        updateUserInfo()
        navigationItem.title = "GinsterJam"
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)
    } 
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        navigationItem.title = ""
    }
    
    //handle default/example text in UITextView
    let initialText = "Write something about your Pasties!"
    func textViewShouldBeginEditing(textView: UITextView) -> Bool{
        if textView.text == initialText {
            textView.text = ""
            textView.textColor = UIColor.darkGrayColor()
        }
        return true
    }
    
    func textViewShouldEndEditing(textView: UITextView) -> Bool{
        if textView.text == "" {
            textView.text = initialText
            textView.textColor = UIColor.lightGrayColor()
        }
        return true
    }
    
    func updateUserInfo(){
        if backend.isLoggedIn() {
            btnLogout.title = "Logout"
        }
        else {
            btnLogout.title = ""
        }
        lblUser.text = backend.loggedInUsername()
    }
    
    
    func dismissImageSource() {
        if (imageSource? != nil) {
            if imageSource!.isViewLoaded() {
                imageSource!.dismissViewControllerAnimated(true, completion: nil)
            }
        }
    }
    
    @IBAction func btnShare_up(sender: AnyObject) {
        self.view.endEditing(true)
        if checkLogin() {
            if validateContent() {    shareImage()    }
        }
    }
    
    func shareImage(){
        var values = Dictionary<String, AnyObject>()
        let imgData = UIImagePNGRepresentation(self.imgPreview.image)
        let imgFile = PFFile(name: "image.png", data: imgData)
        var message = txtMessage.text
        if message == initialText { message = "" }
        
        println(txtMessage.text)
        values["user"] = backend.loggedInUsername()
        values["message"] = message
        values["imageFile"] = imgFile
        backend.addObjectWithValues("Post",values:values,allowPublicRead:true)
    }
    
    func validateContent() -> Bool{
        if imgPreview.image != nil {
            if txtMessage.text == "" || txtMessage.text == initialText {
                var title = "Are you sure?"
                var message = "post image with no message?"
                var alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "cancel", style: .Default, handler: { action in
                    alert.dismissViewControllerAnimated(true, completion: nil)
                }))
                alert.addAction(UIAlertAction(title: "go ahead!", style: .Default, handler: { action in
                    self.shareImage()
                    alert.dismissViewControllerAnimated(true, completion: nil)
                }))
                self.presentViewController(alert, animated: true, completion: nil)
            }
            else { return true }
        }
        else{
            basicAlert("Oh no!", message: "please select an image to post", buttonText: "Ok")
        }
        return false
    }
    
    func checkLogin() -> Bool{
        if backend.isLoggedIn() {
            backend.asyncDelegate = self
            return true
        }
        else {
            var alert = UIAlertController(title: "Oh no!", message: "You're not logged in.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { action in
                alert.dismissViewControllerAnimated(true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "Login", style: .Default, handler: { action in
                alert.dismissViewControllerAnimated(true, completion: nil)
                self.performSegueWithIdentifier("toLogin", sender: self)
            }))
            self.presentViewController(alert, animated: true, completion: nil)
            return false
        }
    }

    @IBAction func btnLogout_up(sender: AnyObject) {
        if backend.isLoggedIn() {
            
            var alert = UIAlertController(title: "Log out of \(backend.loggedInUsername())", message: "are you sure?", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { action in
                alert.dismissViewControllerAnimated(true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "Yes", style: .Default, handler: { action in
                self.backend.logOut()
                self.updateUserInfo()
                alert.dismissViewControllerAnimated(true, completion: nil)
            }))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }

    
    @IBAction func btnPeople_up(sender: AnyObject) {
        //close keyboard
        self.view.endEditing(true)
        //manually triggered segue
        self.performSegueWithIdentifier("toFeed", sender: self)
    }
    
    var imageSource : UIAlertController?
    @IBAction func btnPickImage_up(sender: AnyObject) {
        
        imageSource = UIAlertController(title: "Hey!", message: "Where do you want to source the image?", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        imageSource!.addAction(UIAlertAction(title: "Camera", style: .Default, handler: { action in
            self.beginWait()
            self.pickImage(UIImagePickerControllerSourceType.Camera)
            self.imageSource!.dismissViewControllerAnimated(true, completion: nil)
        }))
        imageSource!.addAction(UIAlertAction(title: "Photos", style: .Default, handler: { action in
            self.beginWait()
            self.pickImage(UIImagePickerControllerSourceType.PhotoLibrary)
            self.imageSource!.dismissViewControllerAnimated(true, completion: nil)
        }))
        imageSource!.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { action in
            self.imageSource!.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        //TODO dismiss image source on tap outside.
        //var tap = UIGestureRecognizer(target: self, action: "dismissImageSource:")
        //tap.cancelsTouchesInView = false
        //        imageSource!.view.superview!.addGestureRecognizer(tap)
        //imageSource.setStyle = UIAlertActionStyle.UIAlertActionStyleCancel
        // UIAlertActionStyleCancel
        
        self.presentViewController(imageSource!, animated: true, completion: nil)
    }
    
    func pickImage(source:UIImagePickerControllerSourceType){
        var picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = source
        picker.allowsEditing = true
        self.presentViewController(picker, animated: true, completion: {self.endWait()})
    }
    
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        imgPreview.image = image
        //remove the placeholder background once we've selected an image
        imgPreview.backgroundColor = UIColor.clearColor()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
//    @IBAction func returnOther(segue:UIStoryboardSegue){}
    

    
    
    var spinny = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50, 50))
    func beginWait(){
        spinny.center = self.view.center
        spinny.hidesWhenStopped = true
        spinny.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        self.view.addSubview(spinny)
        spinny.startAnimating()
    }
    func endWait(){
        spinny.stopAnimating()
    }
    
    
    func beginAsyncWait(callName:String){
        beginWait()
        //this would be a bit crappy if there's no connection!
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    func endAsyncWaitWithSuccess(callName:String, data:[AnyObject]?){
        endWait()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        //the only object add we have is posting an image, so we assume that here (unsafe)
        if callName == "addObjectWithValues" {
            basicAlert("Yay!", message: "image now shared", buttonText: "Ok", closure: {
                self.imgPreview.backgroundColor = self.placeholderColor
                self.imgPreview.image = nil
                self.txtMessage.text = self.initialText
                return nil
            })
        }
    }
    func endAsyncWaitWithError(callName:String, error: NSError){
        endWait()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        
        var errortext = error.userInfo?["error"] as String
        basicAlert("Oh no!", message: errortext, buttonText: "Cancel")
    }
    
    
    func basicAlert(title:String, message:String, buttonText: String){
        basicAlert(title, message: message, buttonText: buttonText, closure: {return nil})
    }
    
    func basicAlert(title:String, message:String, buttonText: String, closure:()->()?){
        var alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: buttonText, style: .Default, handler: { action in
            alert.dismissViewControllerAnimated(true, completion: nil)
            closure()
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }

    
}


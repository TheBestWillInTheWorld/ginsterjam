//
//  TableViewController.swift
//  Ginstergram
//
//  Created by Will Gunby on 22/11/2014.
//  Copyright (c) 2014 Will Gunby. All rights reserved.
//

import UIKit

class UserTableViewController: UITableViewController , UINavigationControllerDelegate, AsyncInteraction {
    
    var backend = BWGinsterJam()
    var users = [String]()
    var watchedUsers = [String]()
    var refreshPull: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        //        navigationController.navigationBar.barTintColor = UIColor.greenColor()
        //self.navigationController!.navigationBar.tintColor = UIColor.redColor()
        
        checkLogin()
        refreshPull = UIRefreshControl()
        refreshPull.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshPull.addTarget(self, action: "refresh", forControlEvents: .ValueChanged)
        self.tableView.addSubview(refreshPull)
    }
    
    var refreshing = false
    override func viewDidAppear(animated: Bool) {
        if refreshing {
            refresh()
        }
    }
        
    func checkLogin() -> Bool{
        if backend.isLoggedIn() {
            backend.asyncDelegate = self
            
            backend.fetchWatchedUsers()
            return true
        }
        else {
            var alert = UIAlertController(title: "Oh no!", message: "You're not logged in.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { action in
                alert.dismissViewControllerAnimated(true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "Login", style: .Default, handler: { action in
                alert.dismissViewControllerAnimated(true, completion: nil)
                self.refreshing = true
                self.performSegueWithIdentifier("toLogin", sender: self)
            }))
            self.presentViewController(alert, animated: true, completion: nil)
            return false
        }
    }
    
    func refresh(){
        refreshing=true
        if checkLogin() {
            backend.fetchWatchedUsers()
            self.refreshPull.endRefreshing()
            refreshing=false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    
    
    
    

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return users.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as UITableViewCell

        cell.textLabel.text = users[indexPath.row]
        if userIsWatched(users[indexPath.row]) {
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
        else {
            cell.accessoryType = UITableViewCellAccessoryType.None
        }

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)! as UITableViewCell
        if cell.accessoryType != UITableViewCellAccessoryType.Checkmark{
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
            backend.addWatch(cell.textLabel.text!)
        }
        else{
            cell.accessoryType = UITableViewCellAccessoryType.None
            backend.deleteWatch(cell.textLabel.text!)
        }
    }
    
    @IBAction func btnBack_up(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func userIsWatched(user:String) -> Bool{
        for watchedUser in watchedUsers{
            if watchedUser == user {return true}
        }
        return false
    }
    
    
    var spinny = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50, 50))
    func beginAsyncWait(callName:String){
        spinny.center = self.view.center
        spinny.hidesWhenStopped = true
        spinny.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        self.view.addSubview(spinny)
        spinny.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    func endAsyncWaitWithSuccess(callName:String, data:[AnyObject]?){
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        spinny.stopAnimating()
        
        if callName == "users" || callName == "fetchUsers"  {
            users = data as [String]
            self.tableView.reloadData()
        }
        if callName == "fetchWatchedUsers" {
            watchedUsers = data as [String]
            backend.fetchUsers()
        }
    }
    func endAsyncWaitWithError(callName:String, error: NSError){
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        spinny.stopAnimating()       
        
        var errortext = error.userInfo?["error"] as String
        var alert = UIAlertController(title: "Oh no!", message: errortext, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
            alert.dismissViewControllerAnimated(true, completion: nil)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
        
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  BW.swift
//  GinsterJam
//
//  Created by Will Gunby on 25/11/2014.
//  Copyright (c) 2014 Will Gunby. All rights reserved.
//

import Foundation
import Parse
import UIKit

class BWGinsterJam: BWBackend {
    
    func addWatch(userToWatch:String){
        var watch = PFObject(className: "Watch")
        watch["watching"] = userToWatch
        watch["user"] = PFUser.currentUser().username
        saveObject(watch, secureObjectToUser: true)
    }
    
    func deleteWatch(userToWatch:String){
        var filter = Dictionary<String, String>()
        
        filter["user"] = PFUser.currentUser().username
        filter["watching"] = userToWatch
        fetch("Watch", filters: filter, closure: deleteItems, callName:"deleteWatch")
    }
    
    func fetchWatchedUsers(callName:String = "fetchWatchedUsers"){
        if asyncDelegate != nil {asyncDelegate?.beginAsyncWait(callName)}
        var query = PFQuery(className: "Watch")
        query.whereKey("user", equalTo: PFUser.currentUser().username)
        
        query.findObjectsInBackgroundWithBlock { (items: [AnyObject]!, error: NSError!) -> Void in
            if error == nil {
                var users = [String]()
                for item in items as [PFObject] {
                    users.append(item["watching"] as String)
                }
                
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithSuccess(callName,data: users)}
            } else {
                //let errorString:String? = error.userInfo["error"]
                // Show the errorString somewhere and let the user try again.
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithError(callName,error: error)}
                
            }
        }
    }
    
    
    func fetchWatchedUserPosts(){
        //bad bad thing.  downloading all image data of watched users ever = loads of data
        //      needs either:
        //              latest post for each user only )that might be a different view altogether
        //              paging of latest posts overall to limit data
        //              load images from Parse on demand as the user scrolls.
        fetch("Post", filters:nil, joinFromField:"user", joinToField:"watching", joinTargetClass:"Watch", joinTargetFilters:["user":loggedInUsername()],callName:"fetchWatchedUserPosts")
    }
}

class BWPost:NSObject {
    var image: UIImage?
    var message: String?
    var user: String?
    var date: NSDate?
    var objId:String!
    
    var index:Int?
    
    var asyncDelegate:AsyncInteraction?
    var pfFile:PFFile?
    
    init(index:Int, parseObject:PFObject, asyncDelegate:AsyncInteraction){        
        println(parseObject)
        self.index = index
        user = parseObject["user"] as? String
        message = parseObject["message"] as? String
        date = parseObject.createdAt
        objId = parseObject.objectId
        pfFile = parseObject["imageFile"] as? PFFile
        self.asyncDelegate = asyncDelegate
        super.init()
    }
    override init(){
        super.init()
    }
    
    func getImageAsync(index:Int, closure:(index:Int, bwPost:BWPost)->(), callName:String = "getImageAsync"){
        
        if asyncDelegate != nil {asyncDelegate?.beginAsyncWait(callName)}
        pfFile!.getDataInBackgroundWithBlock({ (data:NSData!, error: NSError!) -> Void in
            if error == nil {
                self.image = UIImage(data: data)
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithSuccess(callName,data:[data])}
                closure(index:index, bwPost:self)
            }
            else {
                println(error)
                if self.asyncDelegate != nil {self.asyncDelegate!.endAsyncWaitWithError(callName,error: error)}
            }
        })
    }
}
